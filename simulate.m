function [moves, ticks] = simulate(Q, Tmax, nbr_of_particles, start_node)

%generate times
ticks = cumsum(exprnd(1/nbr_of_particles, Tmax, 1));

%Set starting position for each node
particles_pos = start_node*ones(1, nbr_of_particles);

%Generate particles to move
particles_to_be_moved = randi(nbr_of_particles, 1, Tmax);

%Preallocate memory for movements
movements = zeros(1, Tmax);

%Move one particle at the time
for i = 1 : Tmax
    current_particle = particles_to_be_moved(i);
    
    %Get current particles position
    current_node = particles_pos(current_particle);
    
    %Randomly select next node according to Q-matrix
    next_node = find(cumsum(Q(current_node,:)) > rand, 1);
    
    %update position
    particles_pos(current_particle) = next_node;
    
    %save movements
    movements(i) = next_node;
end

%Pack result
moves = [particles_to_be_moved; movements];
end