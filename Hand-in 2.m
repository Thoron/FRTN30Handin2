%%Visualization
clear all
close all
clc

nodes = ['o' 'a' 'b' 'c' 'd'];

% Adjacency matrix 
W = [0 2/5 1/5 0 0;
     0 0 3/4 1/4 0;
     1/2 0 0 1/2 0;
     0 0 1/3 0 2/3;
     0 1/3 0 1/3 0];
 
 %Out degrees
 w = sum(W, 2);
 
 %Q-matrix
 Q = diag(1-w) + W;
 
 %%
 %%%%Calculate return times in part 1
 Tickmax = 1000000;
 nbr_of_particles = 1;
 start_node = 'a';
 
 node = find(nodes == start_node);
 
 %simulate
 [moves, ticks] = simulate(Q, Tickmax, nbr_of_particles, node);

 %calculate average return time
 average = mean(diff(ticks(moves(2,:) == node)));
 
 %calculate true return time
 pi = bonachich(Q);
 pi = pi/norm(pi,1);
 true = 1/pi(node);
 
 %Show result
 disp(['The average return time of node a is ' num2str(average) '.'])
 disp(['The true expected return time is ' num2str(true) '.'])
 disp(' ' )
 %%
 %Calculate hit times in part 1

%Select nodes
start_node = 'o';
end_node = 'd';

%Get node indices
startn = find(nodes == start_node);
endn = find(nodes == end_node);

%Simulate
[moves, ticks] = simulate(Q, Tickmax, nbr_of_particles, startn);

%Find every hit and save clock times
movements = moves(2,:);
start = false;
result = [];
for j = 1 : length(movements)
   if movements(j) == startn && ~start
      start = true;
      count = j;
   end
   
   if movements(j) == endn && start
      result = [result, ticks(j) - ticks(count)];
      start = false;
   end
   
end

%Get average
ave_hit_time = mean(result);

%Calculate true hit time
Qsub = Q;
Qsub(endn, :) = [];
Qsub(:, endn) = []; 
true_hit = (eye(4)-Qsub)\ones(4,1);

%Display answer
disp(['The average hit time from node o to d is ' num2str(ave_hit_time) '.'])
disp(['The true expected hit time is ' num2str(true_hit(startn)) '.'])
disp(' ')
%%
%%%Return time in part 2

%Set parameters
 Tickmax = 200;
 nbr_of_particles = 100;
 start_node = 'a';
 
 %Simulate several times 
for q = 1 : 10000
[moves, ticks] = simulate(Q, Tickmax, nbr_of_particles, find(nodes == start_node));  
s(q) = ticks(find(moves(2,:) == 2, 1));
end

%Display result
disp(['The average time for a particle to return to node a is ' num2str(mean(s)) '.'])
disp(' ')
%%
%Switch to node perspective
clear all
clc

% Adjacency matrix 
W = [0 2/5 1/5 0 0;
     0 0 3/4 1/4 0;
     1/2 0 0 1/2 0;
     0 0 1/3 0 2/3;
     0 1/3 0 1/3 0];
 
 %Out degrees
 w = sum(W, 2);
 
 %Q-matrix
 Q = diag(1-w) + W;
 
 %Set parameters
 Tmax = 60;
 nbr_of_particles = 100;
 start_node = 1;
 mean_max = 100;
 
 %Simulate several times
for i = 1 : mean_max
dist = node_simulate(Q, Tmax, nbr_of_particles, start_node);
d(i, :) = dist(end,:)/norm(dist(end,:), 1);
end
%Print stationary distribution
if mean_max > 1
    disp(['The simulated stationary distribution is '])
    mean(d)
end   
%Caclulate and print true stationary distribution
pi = bonachich(Q)';

disp(['The stationary distribution is '])
pi = pi/norm(pi, 1)


