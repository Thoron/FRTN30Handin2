function particle_dist = node_simulate(Q, Tmax, nbr_of_particles, start_node)

%Initialize nodes and particles
particle_dist = zeros(1, 5);
particle_dist(1, start_node) = nbr_of_particles;

%Run simulation until Tmax is reached
iter = 1;
time = 0;
while time < Tmax
    time = time + exprnd(1/nbr_of_particles);
    iter = iter + 1;
end

%Simulate
for i = 1 : iter
    
    %Select a node randomly
    normed_list = particle_dist(i,:)/norm(particle_dist(i,:), 1);
    selected_node = find(cumsum(normed_list) > rand, 1);
    
    %Select a node to move a particle
    next_node = find(cumsum(Q(selected_node,:)) > rand, 1);
    
    %Update particle list
    particle_dist(i + 1, :) = particle_dist(i, :);
    if selected_node ~= next_node
        particle_dist(i + 1, selected_node) = particle_dist(i, selected_node) - 1;
        particle_dist(i + 1, next_node) = particle_dist(i, next_node) + 1;
    end
    
end
end