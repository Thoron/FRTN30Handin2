function pi = bonachich(W)

%Calculate normalized adjecancy matrix
P = normAdjacency(W);
%Calculate eigenvector corresponding to eigenvalue 1
[pi, ~] = eigs(P', 1);
%Change sign if necessary 
if sum(pi) < 0
    pi = -pi;
end

end